# Литерал
l = []
l = [1, 2, "a"]
l = list()

# Создание на основе другого объекта
s = "a a b c d"
l = s.split()
print(l)
l = list(s)
print(l)

# Генераторы списков
l = [x for x in range(5)]
print(l)
l = list()
for x in range(5):
    l.append(x)
print(l)

# Append
l = [1, 2, 3]
l.append(4)
print(l)

# Extend
l = [1, 2, 3]
l2 = [4, 5, 6]
l.extend(l2)
print(l)
l.extend(l2)
print(l)

# Insert
l = [1, 2, 3, 5]
print(l.index(5))
l.insert(3, 4)
print(l)
print(l.index(5))

# Remove
l = [1, 2, 3]
l.remove(2)
print(l)

# Pop
l = [1, 2, 3, 4]
elem = l.pop(2)
print(f"From list {l} was removed element '{elem}'")
print(elem)
print(l)

# Sort
l = [3, 5, 1, 2, 4, 2, 6, 7]
l_l = ["a", "H", "G", "h"]
l.sort()
l_l.sort()
print(l)
print(l_l)

# Reverse
l = [1, 2, 3]
l.reverse()
print(l)

# Copy
l = [1, 2, 3, 4]
l_new = l.copy()
print(l_new)

# Clear
l = list(range(1, 5))
print(l)
l.clear()
print(l)

#
l = [1, 2, 3, 4]
l_new = l[:]
l.insert(0, 0)
print(l_new)

# Slice
l = [1, 2, 3]
slice = l[:]
print(slice)

# Task
lst1 = [1, 3, 5, 7, 9, 11, 13]
lst2 = [2, 4, 6, 8, 10, 12, 14]
lst1.extend(lst2)
lst1.sort()
# slice = lst1[10:]
slice = lst1[lst1.index(10) + 1:]
print(slice)

# Литералы
t = ()
t = (1, 2)
t = tuple()
t = 1, 2, 3
a = 2  # 3
b = 3  # 2
a, b = b, a

t = (1, "a", 3)

# Set
s = set()
s = {}
print(type(s))

l = [1, 2, 1, 2]
s = "abba"
st = set(l)
print(st)
st = set(s)
print(st)

s = {value for value in range(10)}
print(s)

# Sub set
s_1 = {1, 2, 3}
s_2 = {value for value in range(10)}
print(s_2.issubset(s_1))
print(s_1.issubset(s_2))

print(s_2.issuperset(s_1))
print(s_1.issuperset(s_2))

# Union
s_1 = {1, 2, 3, 4}
s_2 = {1, 3, 5}
s_3 = s_1.union(s_2)
print(s_3)

# InterSec
s_1 = {1, 2, 3, 14, 15, 16}
s_2 = {value for value in range(10)}
print(s_2.intersection(s_1))
print(s_2.difference(s_1))
print(s_1 - s_2)

# Remove
s_1 = {1, 2, 3, 4}
s_1.discard(5)

# Frozen
f = frozenset()

# Dict
d = {}
d = dict()
d = {"name": "John"}

d = dict(name="John", surname="Doe", age=25)
print(d)

d = dict.fromkeys(("a", "b"))
print(d)

d = {x: x ** 3 for x in range(5)}
print(d)

d = dict(name="John", surname="Doe", age=25)
print(d["name"])
d[13] = "some date"
print(d)
d["child"] = dict(name="John", surname="Doe", age=2)
print(d["child"]["name"])

d["children"] = [dict(name="John", surname="Doe", age=2), dict(name="Mary", surname="Doe", age=2)]
import pprint

pprint.pprint(d)
print(d["children"][0]["name"])

d["cars"] = ("volvo", "vw", "toyta")
pprint.pprint(d)

print(d.get("unknown", "value"))

print(d.items())
print(d.values())
print(d.keys())

# Loop
l = [1, 2, 3, 4, 5]
# for index in range(len(l)):
#     print(l[index])
for index, elem in enumerate(l):
    print(f"Index: {index}, value: {elem}")

lst_1 = [1, 3, 5]
lst_2 = [2, 4, 6, 6, 7]
for elem_1, elem_2 in zip(lst_1, lst_2):
    print(f"Element from lst 1: {elem_1}, Element from lst 2: {elem_2}")

d = dict(name="John", surname="Doe", age=25)
d_2 = dict(name="Joh", surname="Do", age=24)

for elem_1, elem_2 in zip(d.items(), d_2.items()):
    print(f"Key: {elem_1}, value: {elem_2}")
