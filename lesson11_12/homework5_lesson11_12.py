"""Homework 5"""

# 1. Сформировать возрастающий список из чётных чисел (количество элементов 23)
# Var 1
# Обозначаем длину списка
list_len = 23
# Создаем список с помощью генератора списков. Учитывая что верхняя граница не входитв список, а шаг 2.
even_list = [even_number for even_number in range(2, (list_len + 1) * 2, 2)]
# Проверяем длину списка
assert len(even_list) == 23

# Var 2
# Обозначаем длину списка
list_len = 23
even_list = [even_number for even_number in range(2, 100, 2)]
even_list_slice = even_list[:list_len]
# Проверяем длину списка
assert len(even_list_slice) == 23

# Var 3
# Обозначаем длину списка
list_len = 23
# Создаем пустой спиков, что бы заполнять его в будующем
even_list = []
# Создаем счетчик
counter = 0
# Идем циклом от 2 до 99
for number in range(2, 100):
    # Если значение счетчика 23, то останавливаем цикл
    if counter == 23:
        break
    # Если число четное, то добавлем егов список и инкрементим счетчик
    if number % 2 == 0:
        even_list.append(number)
        counter = counter + 1
# Проверяем длину списка
assert len(even_list) == 23

# 2. Поменять местами самый большой и самый маленький элементы списка

# Var 1
random_list = [2, 4, 9, 13, 1, 5, 3]
print(f"Before: {random_list}")
# Сортируем список
random_list.sort()
# Меняем позициами первый и посдений элмент, так как список отсортирован
random_list[0], random_list[-1] = random_list[-1], random_list[0]
print(f"After: {random_list}")

# Var 2
random_list = [2, 4, 9, 13, 1, 5, 3]
print(f"Before: {random_list}")
# Ищем максимальное и минимальное значение
min_value = min(random_list)
max_value = max(random_list)
# Меняем позициями, использую метод index()
random_list[random_list.index(min_value)], random_list[random_list.index(max_value)] = random_list[random_list.index(max_value)], random_list[
    random_list.index(min_value)]
print(f"After: {random_list}")


# 3. Создайте функцию is_superset(), которая принимает 2 множества. Результат должен выводиться в консоль одним из сообщений в зависимости от ситуации:
#   1 - «Супермножество не обнаружено»
#   2 – «Объект {X} является чистым супермножеством»
#   3 – «Множества равны»

# Var 1
def is_superset(set_1, set_2):
    if set_1 > set_2:
        print(f'Объект {set_1} является чистым супермножеством')
    elif set_1 < set_2:
        print(f'Объект {set_2} является чистым супермножеством')
    elif set_1 == set_2:
        print('Множества равны')
    else:
        print('Супермножество не обнаружено')


s_1 = {1, 2, 3}
s_2 = {value for value in range(6)}
s_3 = {value for value in range(9)}
is_superset(s_1, s_2)
is_superset(s_3, s_1)


# Var 2 (No much difference)
def is_superset_methods(set_1, set_2):
    if set_1.issuperset(set_2):
        print(f'Объект {set_1} является чистым супермножеством')
    elif set_2.issuperset(set_1):
        print(f'Объект {set_2} является чистым супермножеством')
    elif set_1 == set_2:
        print('Множества равны')
    else:
        print('Супермножество не обнаружено')


s_1 = {1, 2, 3}
s_2 = {value for value in range(6)}
s_3 = {value for value in range(9)}
is_superset(s_1, s_2)
is_superset(s_3, s_1)

# 4. Создайте словарь содержащий в себе 5 блюд (ключи) и их калорийность (значение).
# Добавьте в этот словарь еще одно блюдо и его значение.
# После чего, каждое значение калорийности заменить на кортеж (калорийность, температура подачи).
# Вывести все температуры подачи.
# *Значение калорийности и температуры подачи выбирайте на свое усмотрение, это могут быть как числовые значения,
# так и описание (к примеру "легкий"/"холодный" и тд)

# Создайте словарь содержащий в себе 5 блюд (ключи) и их калорийность (значение).
dishes = {"rice": 100, "potato": 120, "eggs": 200, "meat": 270, "caramel": 350}
# Добавьте в этот словарь еще одно блюдо и его значение.
dishes["onion"] = 30
# После чего, каждое значение калорийности заменить на кортеж (калорийность, температура подачи).
temps = [60, 75, 80, 75, 15, 30]  # Создаем список температур
for key, temp in zip(dishes.keys(), temps):
    dishes[key] = (dishes[key], temp)
# Вывести все температуры подачи.
for key, value in dishes.items():
    print(f"Dish '{key}' has temperature {value[1]}")

# 5.Создать 3-4 словаря с полями имя, возраст и город. Эти словари собрать в список. Программа должны выводить 3 сообщение о каждом словаре следующего формата
# “My … is ...”

# Создать 3-4 словаря с полями имя, возраст и город.
first = {"name": "John", "age": 34, "city": "Kyiv"}
second = {"name": "James", "age": 23, "city": "Oslo"}
third = {"name": "Sjors", "age": 57, "city": "Amsterdam"}
fourth = {"name": "Alex", "age": 76, "city": "Tokio"}
# Эти словари собрать в список.
list_of_dicts = [first, second, third, fourth]
# Программа должны выводить 3 сообщение о каждом словаре следующего формата “My … is ...”
for dct in list_of_dicts:
    for key, value in dct.items():
        print(f"My {key} is {value}")
