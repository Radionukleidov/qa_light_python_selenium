# 1. Создать список из 5 словарей. Каждый из словарей имеет ключи имя и возраст.
# Написать тест, который проверяет что всем участникам списка больше 18 лет.

people = [{"name": "John", "age": 20},
          {"name": "James", "age": 24},
          {"name": "Aaron", "age": 29},
          {"name": "Andrey", "age": 19},
          {"name": "Vasya", "age": 19}]


def test_people_age():
    expected_min_age = 18
    for human in people:
        print(f"Checking {human['name']}")
        assert human["age"] >= expected_min_age, "Всем должно быть больше 18!!!"


def test_people_age_1():
    expected_min_age = 18
    someone_youngest = False
    for human in people:
        print(f"Checking {human['name']}")
        if human["age"] < expected_min_age:
            someone_youngest = True
        # assert human["age"] >= expected_min_age, "Всем должно быть больше 18!!!"
    assert not someone_youngest


def test_people_age_2():
    expected_min_age = 18
    assert any([human["age"] for human in people]) < expected_min_age
    assert all([human["age"] for human in people]) >= expected_min_age
