# 4. Создаем класс Человек. Класс содержит поля:
# - Имя, значение по умолчанию пустая строка
# - Возраст, значение по умолчанию 0.
# Имя нельзя изменить, должна вызыватся ошибка. Возраст можно изменить лишь на единицу в плюс.
# К этому классу написать тесты:
# - Проверяем значения по умолчанию
# - Проверяем изменение возраста
# - Проверяем что имя нельзя изменить
# - Проверяем что неккоректное измение возраста

class Human:

    def __init__(self, name="", age=0):
        self.__name = name
        self.__age = age

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, new_name):
        raise RuntimeError("Имя нельзя менять!!!")

    @property
    def age(self):
        return self.__age

    @age.setter
    def age(self, new_age):
        if new_age == self.__age + 1:
            self.__age = new_age
        else:
            raise ValueError("Возраст можно увеличивать только на 1!")


def test_default():
    human = Human()
    assert human.name == ""
    assert human.age == 0


def test_increase_age():
    human = Human("John", 19)
    human.age = 20
    assert human.age == 20


def test_invalid_update_age():
    human = Human(age=55)
    # Test too much
    try:
        human.age = 60
    except ValueError:
        pass
    else:
        raise AssertionError("Возраст можно увеличивать только на 1!")
    assert human.age == 55

    # Test negative
    try:
        human.age = -30
    except ValueError:
        pass
    else:
        raise AssertionError("Возраст можно увеличивать только на 1!")
    assert human.age == 55


def test_change_name():
    human = Human(name="John")
    try:
        human.name = "James"
    except RuntimeError:
        pass
    else:
        raise AssertionError("Имя нельзя менять!!!")
    assert human.name == "John"
