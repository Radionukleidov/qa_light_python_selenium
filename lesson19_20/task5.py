# 5. Создаем класс Машина. Конструктор принимает поля цвет и год производства.
# Цвет нельзя изменить, при удалении он меняется на цвет по умолчанию.
# Год производства можно свободно менять, но нельзя удалить.
# В классе должен быть метод который возвращает возраст машины на текущий момент.
# К этому классу написать тесты:
import datetime


class Car:

    def __init__(self, year, color="white"):
        self.__color = color
        self.__year = year

    @property
    def color(self):
        return self.__color

    @color.setter
    def color(self, new_color):
        raise RuntimeError("Цвет менять нельзя!")

    @color.deleter
    def color(self):
        self.__color = "white"

    @property
    def year(self):
        return self.__year

    @year.setter
    def year(self, new_year):
        self.__year = new_year

    @year.deleter
    def year(self):
        raise RuntimeError("Год производства удалять нельзя!")

    def age(self):
        return datetime.datetime.now().year - self.year


def test_default_value():
    volvo = Car(year=2001)
    assert volvo.color == "white"


def test_age():
    citroen = Car(year=1995)
    assert citroen.age() == 26
    citroen.year = 1980
    assert citroen.age() == 41
