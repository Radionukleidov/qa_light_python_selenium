# 3. Создать метод который принимает 2 числа. Если хотя бы одно из них не является числом,
# то должна выполняться конкатенация (соединение строк). В остальных случаях введенные числа суммируются.
# К нему написать тесты.

def summa(number1, number2):
    try:
        number1 = int(number1)
        number2 = int(number2)
    except ValueError:
        return f"{number1}{number2}"
    else:
        return number1 + number2


def test_numbers():
    s = summa(1, 4)
    assert isinstance(s, int)


def test_numbers_as_string():
    s = summa("1", "4")
    assert isinstance(s, int)


def test_string():
    s = summa("My ", "name")
    assert isinstance(s, str)


def test_string_and_number():
    s = summa("My age ", 15)
    assert isinstance(s, str)
