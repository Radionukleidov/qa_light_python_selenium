# 2. Создать метод который создавать список из всех четных чисел от 0 до переданого числа.
# Когда счетчик больше 10 работа цикла останавливается.
# К нему написать тесты

def collect_even(number):
    result = []
    for x in range(0, number, 2):
        if len(result) > 10:
            break
        result.append(x)
    return result


def test_count():
    collected = collect_even(100)
    print(collected)
    assert len(collected) == 11


def test_even():
    collected = collect_even(100)
    assert all(collected) % 2 == 0
