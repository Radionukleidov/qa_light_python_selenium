# 6. Создать класс RegisterPage со следующими свойствами:
# - Логин, значние по умолчанию - пустая строка
# - Пароль, значние по умолчанию - пустая строка
# - Повторите пароль, значение по умолчанию - пустая строка
# - Почта, значение по умолчанию - пустая строка
# - sign_up_enabled - значение по умолчанию False
# Значение sign_up_enabled меняется только когда все 4 поля заполненны.
# Логин должен быть длиннее чем 5 символов, иначе ошибка
# Пароль должен содержать не только буквы, иначе ошибка
# Повторите пароль должен быть равен паролю, иначе ошибка.
# Почта должна содержать собачку, иначе ошибка.
# Написать тесты на это класс.

class RegisterPage:

    def __init__(self):
        self.__login = ""
        self.__password = ""
        self.__confirm_password = ""
        self.__email = ""
        self.__sign_up_enabled = False

    @property
    def login(self):
        return self.__login

    @login.setter
    def login(self, new_login):
        if len(new_login) < 5:
            raise ValueError("Логин должен быть длиннее чем 5 символов")
        else:
            self.__login = new_login

    @login.deleter
    def login(self):
        self.__login = ""
        self.__sign_up_enabled = False

    @property
    def password(self):
        return self.__password

    @password.setter
    def password(self, new_password):
        if new_password.isalpha():
            raise ValueError("Пароль должен содержать не только буквы")
        else:
            self.__password = new_password

    @password.deleter
    def password(self):
        self.__password = ""
        self.__sign_up_enabled = False

    @property
    def confirm_password(self):
        return self.__confirm_password

    @confirm_password.setter
    def confirm_password(self, confirm_password):
        if confirm_password != self.__password:
            raise ValueError("Подтверждение пароля должно быть равно паролю")
        else:
            self.__confirm_password = confirm_password

    @confirm_password.deleter
    def confirm_password(self):
        self.__confirm_password = ""
        self.__sign_up_enabled = False

    @property
    def email(self):
        return self.__email

    @email.setter
    def email(self, new_email):
        if "@" not in new_email:
            raise ValueError("Почта должна содержать собачку")
        else:
            self.__email = new_email

    @email.deleter
    def email(self):
        self.__email = ""
        self.__sign_up_enabled = False

    @property
    def sign_up_enabled(self):
        if self.__email and self.__password and self.__confirm_password and self.__login:
            self.__sign_up_enabled = True
        else:
            self.__sign_up_enabled = False
        return self.__sign_up_enabled


class TestRegisterPage:

    def test_defaults(self):
        """
        - Create register page
        - Verify all default values
        """
        register_page = RegisterPage()
        assert register_page.login == ""
        assert register_page.password == ""
        assert register_page.confirm_password == ""
        assert register_page.email == ""
        assert not register_page.sign_up_enabled

    def test_fill_all_fields(self):
        """
        - Create register page
        - Fill all fields
        - Verify that sign_up_enabled became True
        """
        register_page = RegisterPage()

        # Fill fields
        register_page.login = "admin_user"
        register_page.password = "12admin4#7"
        register_page.confirm_password = "12admin4#7"
        register_page.email = "email@mail.com"

        # Verify that button enabled
        assert register_page.sign_up_enabled

    def test_invalid_login(self):
        """
        - Create register page
        - Try to set invalid login
        - Verify that exception exists
        - Verify that login doesn't set
        """
        register_page = RegisterPage()

        # Fill fields
        try:
            register_page.login = "user"
        except ValueError:
            pass
        else:
            raise AssertionError("Логин должен быть длиннее чем 5 символов")

        # Verify that button enabled
        assert not register_page.login
