"""Pytest samples"""


def test_numbers():
    """Verify that 1 equal 1"""
    assert 1 == 1


class TestLetters:
    """Test letters"""

    def test_a_letter(self):
        """Verify a> b"""
        assert "a" > "b"
