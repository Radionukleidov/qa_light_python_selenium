# Практика:
# 1. Создать список из 5 словарей. Каждый из словарей имеет ключи имя и возраст.
# Написать тест, который проверяет что всем участникам списка больше 18 лет.
#
# 2. Создать метод который выводит все четные числа от 0 до переданого числа.
# Когда счетчик больше 10 работа цикла останавливается.
# К нему написать тесты
#
# 3. Создать метод который принимает 2 числа. Если хотя бы одно из них не является числом,
# то должна выполняться конкатенация (соединение строк). В остальных случаях введенные числа суммируются.
# К нему написать тесты.
#
# 4. Создаем класс Человек. Класс содержит поля:
# - Имя, значение по умолчанию пустая строка
# - Возраст, значение по умолчанию 0.
# Имя нельзя изменить, должна вызыватся ошибка. Возраст можно изменить лишь на еденицу.
# К этому классу написать тесты:
# - Проверяем значения по умолчанию
# - Проверяем изменение возраста
# - Проверяем что имя нельзя изменить
# - Проверяем что неккоректное измение возраста
#
# 5. Создаем класс Машина. Конструктор принимает поля цвет и год производства.
# Цвет нельзя изменить, при удалении он меняется на цвет по умолчанию.
# Год производства можно свободно менять, но нельзя удалить.
# В классе должен быть метод который возвращает возраст машины на текущий момент.
# К этому классу написать тесты:
# - Проверяем значение по умолчанию
# - Проверяем изменение года и рекцию метода возраста
# - Проверяем удаление цвета
# - Проверяем удаление года производста
#
# 6. Создать класс РегистрПейд со следующими свойствами:
# - Логин, значние по умолчанию - пустая строка
# - Пароль, значние по умолчанию - пустая строка
# - Повторите пароль, значение по умолчанию - пустая строка
# - Почта, значение по умолчанию - пустая строка
# - sign_up_enabled - значение по умолчанию False
# Значение sign_up_enabled меняется только когда все 4 поля заполненны.
# Логин должен быть длиннее чем 5 символов, иначе ошибка
# Пароль должен содержать не только буквы, иначе ошибка
# Повторите пароль должен быть равен паролю, иначе ошибка.
# Почта должна содержать собачку, иначе ошибка.
# Написать тесты на этот класс.
