# # Read file
# file = open("python_zen.txt")
# lines = [line for line in file]
# print(lines)
# file.close()
#
# # Readlines
# file = open("python_zen.txt")
# lines = file.readlines()
# print(lines)
# file.close()
#
# # Readline
# file = open("python_zen.txt")
# lines = file.readline(20)
# print(lines)
# file.close()

# Delete
# file = open("somefile.txt", "w")
# file.close()
# import os
# if os.path.exists("somefile.txt"):
#     os.remove("somefile.txt")

# Write
file_name = "somefile.txt"
# file = open(file_name, "w")
# file.write("Some text here.\nAnd here :)")
# file.close()
#
# file = open(file_name)
# print(file.read())

# Write list
# lst = [12, 4, 5, 6, 7, 13]
# lst = [str(number) for number in lst]
# file = open(file_name, "w+")
# for number in lst:
#     file.write(str(number) + "\n")
# file.close()
#
# file = open(file_name)
# new_list = [line.replace("\n", '') for line in file]
# file.close()
# print(new_list)
# assert new_list == lst

# Дан файл с текстом (на английском).
# Нужно заменить в нем все “is” на “isn’t” после чего записать в новый файл.
# file = open("python_zen.txt")
# file_data = file.read()
# file.close()
#
# new_file_data = file_data.replace(" is ", " isn't ")
#
# new_file = open("python_zen_new.txt", "w")
# new_file.write(new_file_data)
# new_file.close()

import json

dct = {"people": [
    {"name": "John", "age": 25},
    {"name": "James", "age": 43},
    {"name": "Alice", "age": 57}
]}
# json_dct = json.dumps(dct)
# print(dct)
#
# with open("people.json", "w") as file:
#     file.write(json_dct)
# with open("people.json", "w") as file:
#     json.dump(dct, file)
#
# with open("people.json") as file:
#     json_data = json.load(file)
#     for human in json_data["people"]:
#         print(f"{human['name']} is {human['age']} years old")

# with open("people.json") as file:
#     file_data = file.read()
#     json_data = json.loads(file_data)
#     for human in json_data["people"]:
#         print(f"{human['name']} is {human['age']} years old")


# file_path = "./resources/currency.json"
# with open(file_path) as file:
#     json_data = json.load(file)
#
# new_currency = list()
# for currency in json_data:
#     if float(currency["buy"]) > 30:
#         new_currency.append(currency)
#
# file_path = "./resources/expensive_currency.json"
# with open(file_path, "w") as file:
#     json.dump(new_currency, file)


from lxml import etree

#
# root = etree.Element("xml")  # Define root element
# # Var 1
# elem = etree.Element("child1")  # Create one more element
# elem.set("age", "53")  # Set attribute for child1
# root.append(elem)
#
# # Var 2
# child2 = etree.SubElement(root, "child2")
# child2.text = "Here some text for you"
#
# # Var 3
# child3 = etree.SubElement(root, "child3", attrib={"color": "black", "timeout": "10"})
#
#
# print(etree.tostring(root, pretty_print=True).decode("utf-8"))
#
# with open("new_file.xml", "w") as file:
#     file.write(etree.tostring(root, pretty_print=True).decode("utf-8"))


tree = etree.parse("new_file.xml")
root = tree.getroot()

child1 = root[0]
print(child1.get("age"))

child2 = root.find("child2")
child2.set("age", "23")

with open("new_file_1.xml", "w") as file:
    file.write(etree.tostring(root, pretty_print=True).decode("utf-8"))
