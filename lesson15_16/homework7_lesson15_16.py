"""Homework 7"""

import json
from lxml import etree

# В качестве текстового файла используем python_zen.txt или любой другой файл с текстом на английском.
# Нужно:
#   - Вывести самый популярный символ этого файла.
#   - Вывести предложение с найбольшим колличеством пробелов.
#   - Записать в новый файл весь текст задом на перед.

# Открываем и вычитываем файл
with open("./resources/python_zen.txt") as file:
    file_data = file.read()

# Выводим самый популярный символ
max_count_symbol = file_data[0]
for symbol in file_data:
    if file_data.count(symbol) >= file_data.count(max_count_symbol):
        max_count_symbol = symbol
print(f"Самый популярный символ текста - '{max_count_symbol}'")

# Вывести предложение с найбольшим колличеством пробелов.
sentences = file_data.split("\n")
max_spaces_sentence = sentences[0]
for sentence in sentences:
    if sentence.count(" ") > max_spaces_sentence.count(" "):
        max_spaces_sentence = sentence
print(f"Больше всего пробелов в предложение: '{max_spaces_sentence}'")

# Записать в новый файл весь текст задом на перед.
symbols = list(file_data)
symbols.reverse()
with open("./resources/python_zen_reversed.txt", "w") as file:
    file.writelines(symbols)
print("Данные записаны в новый файл")

# В качестве JSON файла используем currency.json.
# Нужно:
#   - Вывести сообщения о каждой валюте формата "Курс \Х\ на сегодняшний день \Х\ - на покупку и \Х\ на продажу"
#   - Сохранить значение евро и доллара в новый файл.
#   - В оригинальный файл дописать еще одну валюту.

# Открываем и вычитываем файл, сразу выгружаем JSON
with open("./resources/currency.json") as file:
    currencies = json.load(file)

# Вывести сообщения о каждой валюте формата "Курс \Х\ на сегодняшний день \Х\ - на покупку и \Х\ на продажу"
for currency in currencies:
    print(f"Курс {currency['ccy']} на сегодняшний день {currency['buy']} - на покупку и {currency['sale']} на продажу")

# Сохранить значение евро и доллара в новый файл.
eur_and_doll_only = [currency for currency in currencies if currency['ccy'] in ['USD', 'EUR']]
with open("./resources/new_currency.json", "w") as file:
    json.dump(eur_and_doll_only, file)
print("Данные записаны в новый файл")

# В оригинальный файл дописать еще одну валюту.
new_currency = {"ccy": "ETH", "base_ccy": "USD", "buy": "1900", "sale": "2000"}
currencies.append(new_currency)
with open("./resources/currency.json", "w") as file:
    json.dump(currencies, file)
print("Оригинальный файл был изменен")

# Нужно создать (программно) XML файл:
# <root>
#   <students>
#     <student1 age="23" course="5"/>
#     <student2 age="20" course="3"/>
#     <student3 age="19" course="2"/>
#   </students>
#   <universities>
#     <university1 existingCourses="1, 2" name="Brooksberry"/>
#     <university2 existingCourses="3, 4" name="Serbuten" />
#     <university3 existingCourses="5, 6" name="Finestrally"/>
#   </universities>
# </root>
# После чего вывести сообщение о каждом студенте форматат "Студент student1 учится в Brooksberry"

# Создаем дерево
root = etree.Element("root")
students = etree.SubElement(root, "students")
universities = etree.SubElement(root, "universities")

# Students
student1 = etree.SubElement(students, "student1", attrib={"age": "23", "course": "5"})
student2 = etree.SubElement(students, "student2", attrib={"age": "20", "course": "3"})
student3 = etree.SubElement(students, "student3", attrib={"age": "19", "course": "2"})

# Universities
university1 = etree.SubElement(universities, "university1", attrib={"existingCourses": "1,2", "name": "Brooksberry"})
university2 = etree.SubElement(universities, "university2", attrib={"existingCourses": "3,4", "name": "Serbuten"})
university3 = etree.SubElement(universities, "university3", attrib={"existingCourses": "5,6", "name": "Finestrally"})

# Записываем в файл
with open("./resources/universities.xml", "w") as file:
    file.write(etree.tostring(root, pretty_print=True).decode("utf-8"))

# Читаем из файла
tree = etree.parse("./resources/universities.xml")
root = tree.getroot()

# Получаем все универы и всех студентов
universities = root.find("universities")
students = root.find("students")

for student in students:
    for university in universities:
        if student.get("course") in university.get("existingCourses"):
            print(f"Студент {student.tag} учится в {university.get('name')}")
