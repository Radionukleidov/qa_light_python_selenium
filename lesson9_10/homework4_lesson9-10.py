"""
Нужно:
- Вывести количество букв "с"
- Вывести самую “непопулярную” букву (то есть из тех, что есть в тексте, ту что встречается найменьшее колличество раз)
- Найти самое длинное предложение (по количеству букв)
- Найти самое длинное предложение (по количеству слов)
И напомню, не бойтесь тестировать свой код, это то что вы умеете и то что поможет вам найти собственные ошибки. К примеру, в пункте 2 можно добавить какой-то спец символ, что бы он встречался один раз и проверить что программма вернет его.
"""

text = """The Zen of Python, by Tim Peters
Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
Sparse is better than dense.
Readability counts.
Special cases aren't special enough to break the rules.
Although practicality beats purity.
Errors should never pass silently.
Unless explicitly silenced.
In the face of ambiguity, refuse the temptation to guess.
There should be one-- and preferably only one --obvious way to do it.
Although that way may not be obvious at first unless you're Dutch.
Now is better than never.
Although never is often better than *right* now.
If the implementation is hard to explain, it's a bad idea.
If the implementation is easy to explain, it may be a good idea.
Namespaces are one honking great idea -- let's do more of those!
"""

# Task 1 - Вывести количество букв "с"
print(text.count("c"))

# Task 2 - Вывести самую “непопулярную” букву (то есть из тех, что есть в тексте, ту что встречается найменьшее колличество раз)
not_popular_letter = text[0]
only_letters = set()
for symbol in text:
    if symbol.isalpha():
        only_letters.add(symbol)
print(only_letters)

for symbol in only_letters:
    if symbol.isalpha():
        letter_count = text.count(symbol)
        if letter_count <= text.count(not_popular_letter):
            not_popular_letter = symbol
print(not_popular_letter)

not_popular_letters = [text[0]]
for symbol in text:
    if symbol.isalpha():
        if text.count(symbol) < text.count(not_popular_letters[0]):
            not_popular_letters = [symbol]
        elif text.count(symbol) == text.count(not_popular_letters[0]):
            not_popular_letters.append(symbol)
print(not_popular_letters)


# Task 3 - Найти самое длинное предложение (по количеству букв)
def count_letters(txt):
    letters_count = 0
    for symbol in txt:
        if symbol.isalpha():
            letters_count = letters_count + 1
    return letters_count


sentences = text.split('\n')
longest_sentence = sentences[0]
for sentence in sentences:
    current_letters_count = count_letters(sentence)
    if current_letters_count > count_letters(longest_sentence):
        longest_sentence = sentence
print(longest_sentence)

sentences = text.split('\n')
longest_sentences = [sentences[0]]
for sentence in sentences:
    if count_letters(sentence) > count_letters(longest_sentence[0]):
        longest_sentences = [sentence]
    elif count_letters(sentence) == count_letters(longest_sentence[0]):
        longest_sentences.append(sentence)
print(longest_sentences)

# Task 4 - Найти самое длинное предложение (по количеству слов)
sentences = text.split('\n')
longest_sentence = sentences[0]
for sentence in sentences:
    if len(sentence.split(" ")) > len(longest_sentence.split(" ")):
        longest_sentence = sentence
print(longest_sentence)

sentences = text.split('\n')
longest_sentences = [sentences[0]]
for sentence in sentences:
    if len(sentence.split(" ")) > len(longest_sentence[0].split(" ")):
        longest_sentences = [sentence]
    if len(sentence.split(" ")) == len(longest_sentence[0].split(" ")):
        longest_sentences.append(sentence)
print(longest_sentences)
