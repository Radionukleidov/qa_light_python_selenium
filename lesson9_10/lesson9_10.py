"""Lesson 9 10"""


class Lesson_9_10:
    pass


# s = "Somet \" hing"
# print(s)

# s = "String \nnew string"
# print(s)
#
# s = "\ttab string"
# print(s)

# s = "\tstring\nnew string\n\tstring"
# print(s)

# s = '''string
# string
#     string'''
# print(s)

# name = "Name"
# surname = " Surname"
# full_nam = name + surname
# print(full_nam)

# print("name" * 100)

# hund_names = "name" * 100
# print(len(hund_names))

# s = "Index"  # ("I", "n", "d", "e", "x")
# print(s[0])  # I
# print(s[-1])  # x

# print(s[1:3])  # nd

# new_s = s[:]  # срез переменной - считывание полностью строки в новую переменную
# print(new_s)
# print(s[2:])  # dex
# print(s[:2])  # In

# s = "Some index here, some index here, some index here"
# index = s.find("h")
# print(index)
# print(s[11])
# index_2 = s.find("here")
# print(index_2)
# index_3 = s.find("here", 1, 10)
# print(index_3)  # вернет -1 - это ошибка, что не найден то что искали
# last_index = s.rfind("here")
# print(last_index)


# Index

# s = "abcdefg"
# index = s.index("b")
# print(index)
# index = s.index("w")
# print(index)  # вернет ValueError: substring not found

# Replace

# s = "My name is Jhon. My best friend is Jhon. My father is Jhon"
# new_s = s.replace("Jhon", "James", 2)  # 2 - количество замен
# print(s)
# print(new_s)

# Split

s = "Hello? my name is"
print(s.split())  # в список
words = s.split()
print(len(words))

# Is

# s = "353253252r"
# is_digit = s.isdigit()
# print(is_digit)

# lower/upper

# s = "Hello, my name is"
# print(s.upper())
# print(s.lower())

# >>with

# s = "Hello, my name is"
# print(s.startswith("Hello"))
# print(s.endswith("Maks"))

# Join

# new_s = ", ".join(("a", "b", "c"))  # , - разделитель
# print(new_s)
# new_s_s = "-letter ".join(("a", "b", "c"))  # -letter - разделитель
# print(new_s_s)


# Cap

# s = "name"
# new_s = s.capitalize()
# print(new_s)

# # Count
# s = "Name Name is Name Name"
# count = s.count("a")
# print(count)
# s = "Name name is Name name"
# count_lower = s.count("n")
# print(count_lower)
# count_all = # не успел
# print(count_all)

# Strip - удаление пробелов

# s = "    name    "
# print(s)
# new_s = s.strip()
# print(new_s)

# Самостоятельная

# """Из строки “Hello! My name John. Nice to meet you!” сделать список ['H*llo!', 'My', 'nam*', 'John.', 'Nic*', 'to',
# 'm**t', 'you!']"""
#
# s = "Hello! My name John. Nice to meet you!"
# expected = "['H*llo!', 'My', 'nam*', 'John.', 'Nic*', 'to', 'm**t', 'you!']"
# actual = s.replace("e", "*").split()
# print(actual)
# print(actual == expected)
# assert actual == expected

# Formatting
# type 1
# print("Input your name")
# name = input()
# print("Input your surname")
# surname =input()
# print("Hello, {surname} {name}".format(name=name, surname=surname))
#
# print("My age is {}".format(15.0))

# type 2
# s = "Hello, my name is %s" % "Jhon"
# print(s)
# s = "My age is %f" % 10.0
# print(s)


# if __name__ == "__main__":  # будет вызываться только если будет вызываться непосредественно при обращении напрямую
#     # type3
#     print("Input your name")
#     name = input()
#     print("Input your surname")
#     surname = input()
#     s = f"My name is {name} {surname}"
#     print(s)
#     age = 30
#     age_days = 1238123.31312
#     s_s = f"My age is {age} and days {age_days}"
#     print(s_s)


# в консоли import this

"""
The Zen of Python, by Tim Peters
Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
Sparse is better than dense.
Readability counts.
Special cases aren't special enough to break the rules.
Although practicality beats purity.
Errors should never pass silently.
Unless explicitly silenced.
In the face of ambiguity, refuse the temptation to guess.
There should be one-- and preferably only one --obvious way to do it.
Although that way may not be obvious at first unless you're Dutch.
Now is better than never.
Although never is often better than *right* now.
If the implementation is hard to explain, it's a bad idea.
If the implementation is easy to explain, it may be a good idea.
Namespaces are one honking great idea -- let's do more of those!
"""