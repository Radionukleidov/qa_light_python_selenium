"""Lesson 7 8"""


class Planet:

    def __init__(self, weight):
        self.weight = weight

    def print_weight(self):
        print("Planet weight: " + str(self.weight))

    def count_satelite(self):
        count = self.weight / 100
        self.__planet()
        return count

    def __planet(self):
        print("I'm planet")


class Venus(Planet):

    def __init__(self, weight, radius):
        super().__init__(weight)  # Initializing Planet - Planet(weight)
        self.radius = radius  # Initializing Venus..

    def print_radius(self):
        print("Planet radius: " + str(self.radius))
        self.__planet()


class Earth(Venus):
    def count_satelite(self):
        count = self.weight / 1000
        return count


earth = Earth(1000, 44000)
some_planet = Planet(1000)
venus = Venus(1000, 30000)


# print(some_planet.count_satelite())
# print(venus.count_satelite())
# print(earth.count_satelite())

# earth.print_weight()
# satelite_number = earth.count_satelite()
# print(satelite_number)
# earth.print_radius()

# some_planet.count_satelite()


class Numbers:
    def __init__(self, number):
        self.__number = number

    @property
    def number(self):
        return "Hi! Number is " + str(self.__number)

    @number.setter
    def number(self, new_number):
        if new_number < 200:
            print("Number is to small")
        else:
            self.__number = new_number

    @number.deleter
    def number(self):
        print("Who are you ?")
        user = input()
        if user == "admin":
            self.__number = 0
        else:
            print("Sorry, you're not admin")


# numbers = Numbers(465)
# print(numbers.number)
# numbers.number = 150
# print(numbers.number)
# numbers.number = 500
# print(numbers.number)
# del numbers.number
# print(numbers.number)
# del numbers.number
# print(numbers.number)


class UserName:

    def __init__(self, username):
        self.__username = username

    @property
    def username(self):
        return "Username: " + self.__username

    @username.setter
    def username(self, new_name):
        if "#" in new_name:
            print("Error")


class Default:

    def __init__(self, number):
        self.number = number


# d = Default(56)
# print(d.number)
# d.number = 150
# print(d.number)


class Human:

    def __init__(self, age):
        self.age = age

    def life(self):
        print("Я живу " + str(self.age) + " лет")


class Worker(Human):

    def __init__(self, age, salary):
        super().__init__(age)
        self.salary = salary

    def life(self):
        print("Я живу и работаю " + str(self.age) + " лет, с зарплатой " + str(self.salary))


human = Human(35)
worker = Worker(35, 1000)
human.life()
worker.life()
