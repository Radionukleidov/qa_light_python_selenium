"""
1. Создать метод который принимает число строкой (можно использовать input()) и int.
Если строку не удается конвертировать в int, то выводить сообщение "Вы что-то попутали с вводом".
"""


def validate_number(number_str):
    """Validate is string integer number"""
    try:
        number = int(number_str)
    except ValueError:
        print("Вы что-то попутали с вводом")
    else:
        print(f"Ваше значение {number} в порядке")


print("Введите любое число...")
string_value = input()
validate_number(string_value)

"""
2. Создать метод который принимает 2 числа.
Если хотя бы одно из них не является числом, то должна выполняться конкатенация (соединение строк).
В остальных случаях введенные числа суммируются.
"""


def variables_sum(number_str_1, number_str_2):
    """Sum numbers as number otherwise as a string"""
    try:
        number_1 = float(number_str_1)
        number_2 = float(number_str_2)
    except ValueError:
        return number_str_2 + number_str_1
    else:
        return number_1 + number_2


print("Введите первое значние:")
variable1 = input()
print("Введите второе значние:")
variable2 = input()
print(variables_sum(variable1, variable2))

"""
3. Создать собственный класс, объекты которого можно будет вызывать с помощью контекстного менеджера.
Итоговая программа должна выводить:
  Nice to meet you here - вывод из метода входа
  We're inside !!! - непосредственный вывод
  Have a good one! - вывод из метода выхода
"""


class SampleOutput:

    def __enter__(self):
        print("Nice to meet you here!")

    def __exit__(self, exc_type, exc_val, exc_tb):
        print("Have a good one!")


out = SampleOutput()
with out:
    print("We're inside!!!")
