assert True
# assert 10 == 7, "Some message"

# Attribute Error
class Base:
    pass

b = Base()
# b.func()

# Import Error
# import SomeRemoteLib
# print(a)

l = [12, 3]
l2 = [45, 67]
# print(l - l2)


# Try/Except
# l = [1, 2]
# d = {"a": 1}
# l.append(4)
# try:
#     print(l[1])
# except IndexError:
#     print(f"Last index:{len(l) - 1}, you tried 6")
# l.append(6)
# print(l)
# try:
#     x = 100/0
#     # x = d["b"]
# except (ZeroDivisionError, IndexError) as exception:
#     print(exception)


# Else/Fianlly
# l = [1, 2]
# l.append(4)
# try:
#     print(l[1])
# except IndexError:
#     print(f"Last index: {len(l) - 1}, you tried 6")
# else:
#     print("No error today, yeah!")
# finally:
#     l.append(6)
#     print(l)
#
# age = -1
# try:
#     assert age < 0
# except:
#     pass
# else:
#     raise RuntimeError("Negative age value")
# finally:
#     age = 0
#     print(age)

age = 10
print(age)

# responses
# response = 403
# expected = 200
# try:
#     assert response == expected, f"Actual: {response}, Expected: {expected}"
# except IndexError:
#     response = 200


# with
# file = open("lesson3_4.py")
# try:
#     print(file[10101001])
# finally:
#     file.close()
#
# with open("lesson3_4.py") as file:
#     print(file[109000000])

# number = None
# while not number:
#     print("Input some number:")
#     try:
#         number = int(input())
#     except:
#         print("Ввведи все таки цифру...")
#
# try:
#     assert number % 2 == 0
#     print("Ваше число четное")
# except AssertionError as exp:
#     print("Ваше число не четное")
# finally:
#     print("Спасибо за использование")

class Human:

    def __init__(self, name, age):
        self.age = age
        self.name = name

    def __enter__(self):
        print("Enter surname")
        self.surname = input()
        print(f"My name is {self.name}, I'm {self.age} years old")

    def __exit__(self, exc_type, exc_val, exc_tb):
        del self.surname
        print("Thanks, buy!")

human = Human("John", 45)

with human:
    print(f"Hello, I'm {human.name} {human.surname}")
    try:
        print(human.salary)
    except AttributeError as exception:
        print(exception)
    print(human.age)

print(human.name)

