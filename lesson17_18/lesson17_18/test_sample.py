"""Pytest samples"""
import pytest

letters = ["a", "b"]

def test_numbers():
    """Verify that 1 equal 1"""
    assert 1 == 1


class TestLetters:
    """Test letters"""

    def test_a_letter(self):
        """Verify a> b"""
        print("Hello")
        assert "a" < "b"

    @pytest.mark.parametrize(*letters)
    def test_a_letters(self, letter):
        """Verify a> b"""
        print("Hello")
        assert letter < "b"
