# import datetime
# import unittest
#
#
# class BaseTest(unittest.TestCase):
#     """
#     General test class
#     """
#     @classmethod
#     def setUpClass(cls):
#         """
#         Setup before run tests from class
#         """
#         print("\nInitializing class level variables...")
#
#     @classmethod
#     def tearDownClass(cls):
#         """
#         Clean up after class tests run
#         """
#         print("\nCleanup once after class")
#
#
# class DateTests(BaseTest):
#     """
#     Test class
#     """
#     def setUp(self):
#         """Run before each test"""
#         print("\nInitializing method level variables...")
#         self.now = datetime.datetime.now()
#
#     def tearDown(self):
#         """Run after each test"""
#         del self.now
#         print("\nClean up after each test")
#
#     def test_is_2021(self):
#         self.assertEqual(self.now.year, 2021)
#         # assert self.now.year == 2021
#         print("Year is verified")
#
#     def test_is_not_summer(self):
#         self.assertNotIn(self.now.month, [4, 7, 8])
#         # assert  not in
#         print("Season was verified")
#
