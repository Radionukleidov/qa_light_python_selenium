"""Homework 8"""


# Создать класс с соседствующими элементам:
# - Свойство sign_in_enabled(bool). Значение по умолчанию - False.
# - Свойство login(str). Значение по умолчанию - пустая строка.
# - Свойство password(str). Значение по умолчанию - пустая строка.
# - Если оба поля не являются пустыми то sign_in_enabled должен автоматически становится True.
# *Подсказка: тут нужно будет использовать @property для обновления 'sign_in_enabled'
import pytest


class LoginPage:

    def __init__(self, login="", password=""):
        self.__sign_in_enabled = False
        self.__login = ""
        self.__password = ""
        self.login = login
        self.password = password

    @property
    def login(self):
        return self.__login

    @login.setter
    def login(self, user_name):
        self.__login = user_name
        if self.__login and self.__password:
            self.__sign_in_enabled = True
        else:
            self.__sign_in_enabled = False

    @login.deleter
    def login(self):
        self.__login = ""
        self.__sign_in_enabled = False

    @property
    def password(self):
        return self.__password

    @password.setter
    def password(self, user_password):
        self.__password = user_password
        if self.__login and self.__password:
            self.__sign_in_enabled = True
        else:
            self.__sign_in_enabled = False

    @password.deleter
    def password(self):
        self.__password = ""
        self.__sign_in_enabled = False

    @property
    def sing_in_enabled(self):
        return self.__sign_in_enabled


# Написать тесты:
# - Тест проверяющий что пока текст в полях отсутствует кнопка Sign in недоступна (изначальное состояние)
# - Тест проверяющий, что если добавить значение для одного из полей, то кнопка все еще не доступна.
# - Тест проверяющий, что если добавить значения в оба поля, то кнопка доступна.
# А также если удалить значение у одного из полей, то кнопка перестает быть доступной.

class TestLoginPage:
    """Class stores all tests related to login page"""

    def test_default_state(self):
        """
        - Create LoginPage object
        - Verify that sign_in_enabled is False
        """
        # Create LoginPage object
        login_page = LoginPage()

        # Verify that sign_in_enabled is False
        assert not login_page.sing_in_enabled

    @pytest.mark.parametrize("field", ("login", "password"))
    def test_set_only_one_field(self, field):
        """
        - Create LoginPage object
        - Set only password
        - Verify that sign_in_enabled still False
        """
        # Create LoginPage object
        login_page = LoginPage()

        # Set only password
        # 1. login_page.login = "12345"
        # 2. login_page.password = "12345"
        setattr(login_page, field, "12345")

        # Verify that sign_in_enabled still False
        assert not login_page.sing_in_enabled

    def test_fill_all_fields(self):
        """
        - Create LoginPage object
        - Set login and password
        - Verify that sign_in_enabled is True
        - Delete login value
        - Verify that sign_in_enabled became False
        """
        # Create LoginPage object with login and password
        login_page = LoginPage("admin", "1234")

        # Verify that sign_in_enabled is True
        assert login_page.sing_in_enabled

        # Delete login value
        del login_page.login
        assert login_page.login == ""

        # Verify that sign_in_enabled became False
        assert not login_page.sing_in_enabled
