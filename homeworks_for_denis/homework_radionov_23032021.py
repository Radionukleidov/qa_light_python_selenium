"""
Homework for 23/03/2021

     Берем текст из вывода import this, сохраняем его в строку. Нужно:
    - Вывести количество букв "с"
    - Вывести самую “непопулярную” букву (то есть ту что встречается найменьшее колличество раз)
    - Найти самое длинное предложение (по количеству букв)
    - Найти самое длинное предложение (по количеству слов)

"""

text = """The Zen of Python, by Tim Peters.
Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
Sparse is better than dense.
Readability counts.
Special cases aren't special enough to break the rules.
Although practicality beats purity.
Errors should never pass silently.
Unless explicitly silenced.
In the face of ambiguity, refuse the temptation to guess.
There should be one -- and preferably only one -- obvious way to do it.
Although that way may not be obvious at first unless you're Dutch.
Now is better than never.
Although never is often better than *right* now.
If the implementation is hard to explain, it's a bad idea.
If the implementation is easy to explain, it may be a good idea.
Namespaces are one honking great idea -- let's do more of those!
"""


class StringHomework:
    """ Class for homework fo 32\03\2021 - strings """

    @staticmethod
    def this_count_c():
        """ Count of 'c' letter. """
        count = text.count('c')
        print(f"Буквы 'c' в тексте {count} штук")

    @staticmethod
    def alpabet_letter_with_end_this():
        """ :return: alphabet list with "." and "!" for definition end of sentence. """
        alpabet = []
        for i in text:
            if i.isalpha():
                alpabet += i
            elif i == "." or i == "!":  # идем по значениям списка, и если это . или !
                alpabet += i
        return alpabet

    @staticmethod
    def clear_sentences_this():
        """ :return: clear alphabetical text without symbols but with dots and spaces. """
        this_clear_pref = text.replace("--", "")
        this_without_dot = this_clear_pref.replace(".", " .")
        this_clear_gape = this_without_dot.replace("\n", " ")
        this_without_voskl = this_clear_gape.replace("!", " !")
        return this_without_voskl

    @staticmethod
    def rare_letter():
        """ Find more rare letter in the text. """
        new_dict = {}  # создаем пустой дефолтный словарь
        print("Наименее встречающиеся буквы в тексте: ")
        for i in text:  # для каждого элемента из текста
            if i.isalpha():  # если он буква
                if i not in new_dict:  # если его еще нет в словаре
                    new_dict[i] = 1  # устанавливаем значение 1
                else:
                    new_dict[i] += 1  # иначе если есть - добавляем 1 к существующему значению
        min_dict = min(new_dict.values())  # находим минимальное значение в словаре
        for keys, values in new_dict.items():
            if values == min_dict:  # если значение == минимальному значению
                print(f"Буква {keys} - {values} раз")  # печатаем

    @staticmethod
    def longest_sentence_by_letter():
        """ Definition longest sentence by letter count. """
        search_item = "букв"  # в стандартизированном выводе выводить букв
        this_for_search = StringHomework.alpabet_letter_with_end_this()
        StringHomework.find_longest_sentence_by(search_item, this_for_search)

    @staticmethod
    def longest_sentence_by_words():
        """ Definition longest sentence by word count. """
        search_item = "слов"  # в стандартизированном выводе выводить слов
        this = StringHomework.clear_sentences_this()
        this_for_search = this.split()
        StringHomework.find_longest_sentence_by(search_item, this_for_search)

    @staticmethod
    def find_longest_sentence_by(search, this_for_search):
        """ Standard method for find longest sentence in the list by letter or word. """
        list_of_text = []  # дефолтный список предложений
        list_for_comparison = []  # дефолтный список для сравнения длины
        count = 0  # дефолтное значение количества слов\букв
        if search == "слов":
            print(f"Самое длинное предложение по количеству {search}: ")
        elif search == "букв":
            print(f"Самое длинное предложение по количеству {search} (выводится в виде букв предложения): ")
        for i in this_for_search:
            list_of_text.append(i)  # добавляем в дефолтный список слово\букву
            count += 1  # увеличиваем количество слов\букв на 1
            if i == "." or i == "!":  # идем по значениям списка, и если это .  или ! = значит конец предложения!
                list_for_comparison.append(
                    list_of_text)  # добавляем в дефолтный список для сравнения список с предложением
                list_of_text = []  # обнуляем дефолтный список
                count = 0  # обнуляем дефолтный счетчик
        max_items_of_search = (max(list_for_comparison, key=len))  # находим максимально длинный список
        len_max_items = len(max_items_of_search) - 1  # узнаем длину самого длинного предложения
        for sentence in max_items_of_search:  # красиво печатаем предложение с учетом того буквы или слова ищем
            if search == "слов":
                print(f"{sentence}", end=" ")
            elif search == "букв":
                print(f"{sentence}", end="")
        print(f"\nКоличество {search} в самом большом предложении - {len_max_items}")


print("=" * 30)
StringHomework.this_count_c()  # Вывести количество букв "с"
print("=" * 30)
StringHomework.rare_letter()  # Вывести самую “непопулярную” букву
print("=" * 30)
StringHomework.longest_sentence_by_words()  # Найти самое длинное предложение (по количеству слов)
print("=" * 30)
StringHomework.longest_sentence_by_letter()  # Найти самое длинное предложение (по количеству букв)
print("=" * 30)
