"""
Homework for 16/03/2021
"""


class Homework:
    """Class for homework from 16-03-2021. Has no class fields.
    Описать предыдущую домашку в ввиде класса, где каждое задание - это отдельный метод.
    То есть в результате должна быть возможность вызвать каждое задание.
    Типо: homework.task1() и т.д"""

    @staticmethod
    def get_sum_numbers():
        """
        Create a list with numbers. We create a dictionary where values are numbers, keys of your choice are
        number / string. Print the sum of element X of the dictionary and the last element of the list.
        """
        list_int = [1, 2, 3, 4, 5]
        dict_int = {1: 1, 2: 2, 3: 3, 'X': 4, 5: 5}
        sum_dict_list = dict_int['X'] + list_int[-1]
        print('\nЗадание # 1')
        print(
            f'\nСумма элемента Х словаря (X = {dict_int["X"]}) и последнего элемента списка ({list_int[-1]}): '
            f'{dict_int["X"]} + {list_int[-1]} = {sum_dict_list}')
        print("=" * 30)

    # Homework.get_sum_numbers()

    @staticmethod
    def get_division_result():
        """
        There is a list where the first element is a number represented by a string, and the second is an
        integer numeric value. Display the result division as an integer numeric value (type must be int)
        """
        strange_list = ['11', 3]
        division = int(int(strange_list[0]) / strange_list[1])
        print('\nЗадание # 2')
        print(
            f'\nРезультат деления {strange_list[0]} на {strange_list[1]} в виде целого числового значения = {division}')
        print("=" * 30)

    # Homework.get_division_result()

    @staticmethod
    def get_retire_age():
        """
        The program displays 3 messages. “It's time to retire” if the person is over 70, “Wait a bit” if the person is
        over 65 and “No, you still have to work and work” if the person is under 65. Age is entered from the command
        line. 70 is included in "time to retire", 65, respectively, in "wait a bit."
        """
        print('\nЗадание # 3')
        age = int(input('\nПроверим, а не пора ли тебе на пенсию! Ану-ка, введи свой возраст:  '))
        if age < 65:
            print('\nНет, тебе еще работать и работать')
        elif 65 <= age < 70:  # Есть вопросы по граничным значениям
            print('\nПодожди чуток')
        elif age >= 70:
            print('\nПора на пенсию')
        print("=" * 30)

    # Homework.get_retire_age()

    @staticmethod
    def get_even_numbers():
        """
        Bonus task:

        The program outputs all even numbers from 0 to the entered number. When the counter is greater than 10, the
        cycle stops.
        Do with a for loop.
        """
        print('\nБонусное задание # 1')
        your_input = int(input('\nВведи любое целое число: '))
        print(f"\nВсе четные числа от 0 до {your_input} ({your_input} - НЕ ВКЛЮЧИТЕЛЬНО, максимум 10!):")
        for i in range(your_input):
            if i > 10:
                break
            elif i % 2 != 0:
                continue
            print(i)
        print("=" * 30)

    # Homework.get_even_numbers()

    @staticmethod
    def count_of_multiple_of_five():
        """
        Create a method that counts the number of multiples of 5 between 0 and a given number (the number is
        entered from the console).
        """
        print('\nБонусное задание # 2')
        count = 0
        your_input = int(input('\nВведи любое целое число: '))
        for i in range(0, your_input):
            if i == 0:
                continue
            elif i % 5 == 0:
                count += 1
        print(f'\nKоличество чисел кратных "5" между 0 и {your_input} = ', count)
        print("=" * 30)

    # Homework.count_of_multiple_of_five()


Homework.get_sum_numbers()
Homework.get_division_result()
Homework.get_retire_age()
Homework.get_even_numbers()
Homework.count_of_multiple_of_five()


class MathActions:
    """
    Создать класс который при инициализации принимает число. Имеет методы для вычисления корня и квадрата числа.
    Инициализировать класс с числом введенным из консоли, после чего вызвать методы и вывести результат их работы.
    """

    def __init__(self, input_number):
        self.number = input_number

    def square_root(self):
        """Get number sqrt"""
        return self.number ** 0.5

    def x2(self):
        """Get number kvadrat"""
        return self.number ** 2


initializing = MathActions(int(input("Input some number: ")))
square_root = initializing.square_root()
print(square_root)
x2 = initializing.x2()
print(x2)
