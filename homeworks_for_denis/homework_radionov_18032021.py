"""
Homework for 18/03/2021
"""


class Worker:
    """
    1. За основу берем класс Worker из лекции.
    В класс Работник добавить поле профессия. Данное поле должно быть приватным и доступным через @property.
    При изменении данного поля должно выводится сообщение “Профессия  была изменена с .. на …”.
    При удаление данного поля, свойство должно получать значение “Unknown”.
    """

    def __init__(self, salary, profession):
        self.salary = salary
        self.__profession = profession

    @property
    def profession(self):
        """ Данное поле должно быть приватным и доступным через @property. """
        return "Profession: " + str(self.__profession)

    @profession.setter
    def profession(self, new_profession):
        """ При изменении данного поля должно выводится сообщение “Профессия  была изменена с .. на …”. """
        if self.__profession != new_profession:
            print(f"Профессия была изменена с {self.__profession}  на {new_profession}")

    @profession.deleter
    def profession(self):
        """ При удаление данного поля, свойство должно получать значение “Unknown”. """
        input_profession = input("Удалить профессию? Введите Да, если хотите удалить, либо Нет - если не хотите ....  ")
        if input_profession == "Да":
            del self.__profession
            print("Удалено")
            self.__profession = "Unknown"
        elif input_profession == "Нет":
            print("Ну нет так нет")
        else:
            print("Ни Да ни Нет - вот ваш ответ!")


worker = Worker(1000, 'QA')
print(worker.profession)
worker.salary = 2200
print(worker.salary)
worker.profession = 'AQA'
print(worker.profession)
del worker.profession
print(worker.profession)
print("=" * 30)
print("=" * 30)


class Car:
    """
    2. Создать класс Car. Класс должен принимать свойства цвет и вес.
    У цвета при инициализации должно быть значение по умолчанию, к примеру white - у меня дефолтный цвет - brown.
    Цвет нельзя изменить, но при удалении цвет должен меняться на значение по умолчанию.
    Вес нельзя ни изменить, ни удалить.
    При обращение к весу должно возращать "Машина весит /Х/ тонн".
    """

    def __init__(self, weight, color='brown'):
        """ Класс должен принимать свойства цвет и вес """
        self.__color = color
        self.__weight = weight

    @property
    def color(self):
        return f"Машина имеет цвет по каталогу - {self.__color}"

    @color.deleter
    def color(self):
        """ Цвет нельзя изменить, но при удалении цвет должен меняться на значение по умолчанию. """
        del self.__color
        self.__color = 'brown'

    @property
    def weight(self):
        """
        Вес нельзя ни изменить, ни удалить.
        При обращение к весу должно возращать "Машина весит /Х/ тонн".
        """
        return print(f"Машина весит {str(self.__weight)} тонн")


car = Car(1.4)
print(car.color)
weight = car.weight
del car.color
print(car.color)
print("=" * 30)
print("=" * 30)


class Child:
    """
    3. Создать класс Child.
    Класс должен иметь свойства возраст и имя.
    Возраст нельзя поменять больше чем на единицу за раз (и только в плюс), а при обращении он должен выводить
    “/Имя/ на данный момет … лет” после чего возвращать значение.
    От Child наследовать еще 2 класса Student и Schoolboy.
    Оба этих класса должны иметь метод study(), который будет выводить “Мне .. лет и я учусь в школе/универе”.
    """

    def __init__(self, age, name):
        """ Класс должен иметь свойства возраст и имя. """
        self._age = age
        self.name = name

    @property
    def age(self):
        """ При обращении он должен выводить “/Имя/ на данный момет … лет” после чего возвращать значение """
        return f"{self.name} на данный момент {self._age} лет"

    @age.setter
    def age(self, new_age):
        """ Возраст нельзя поменять больше чем на единицу за раз (и только в плюс) """
        if new_age < int(self._age):
            print("Нельзя делать возраст меньше")
        elif new_age > (int(self._age) + 1):
            print("Нельзя делать возраст больше чем на 1")
        elif new_age == self._age + 1:
            self._age = new_age
            print(f"Новый возраст: {new_age}")
        elif new_age == self._age:
            print(f"Возраст не изменился")


class Student(Child):
    def __init__(self, age, name):
        super().__init__(age, name)

    def study(self):
        """ Метод study(), который будет выводить “Мне .. лет и я учусь в универе” """
        print(f"Мне {self._age} лет и я учусь в универе")


class Schoolboy(Child):
    def __init__(self, age, name):
        super().__init__(age, name)

    def study(self):
        """ Метод study(), который будет выводить “Мне .. лет и я учусь в школе” """
        print(f"Мне {self._age} лет и я учусь в школе")


child = Child(10, 'Микола')
print(child.age)
child.age = 55
print("=" * 30)
student = Student(18, 'Гретта')
print(student.age)
student.study()
student.age = 19
print("=" * 30)
school = Schoolboy(15, 'Богдана')
print(school.age)
school.study()
school.age = 11
print("=" * 30)
print("=" * 30)


class QualityAssuranceEngineer:
    """
    4. Создать класс QualityAssuranceEngineer.
    Конструктор принимает параметры опыт (в годах) и зарплата.
    Опыт можно увеличивать максимум на 2 года и только в плюс.
    При изменение запрплаты выводить радостное сообщение, если больше чем было и грустное, если меньше.
    Класс содежит метод do_testing, котрый выводит "Начинаю тестирование...". От него
    отнаследовать класс ManualQualityAssuranceEngineer.
    В нем переопределить метод do_testing, он должен выводить "Эх, погнал тестировать руками...".
    Также от основного класса отнаследовать класс AutomationQualityAssuranceEngineer.
    Его конструктор должен принимать дополнительный параметр code_lang.
    Метод do_testing переопределить что бы он выводил "Ха, щя поставлю раниться тесты на /code_lang/ и го пить кофе )".
    """

    def __init__(self, experience, salary):
        """ Конструктор принимает параметры опыт (в годах) и зарплата. """
        self._experience = experience
        self._salary = salary

    @property
    def experience(self):
        return f"Текущий опыт подопытного - {self._experience} (в годах)"

    @experience.setter
    def experience(self, new_year_experience):
        """ Опыт можно увеличивать максимум на 2 года и только в плюс. """
        if new_year_experience == self._experience + 1 \
                or new_year_experience == self._experience + 2:
            self._experience = new_year_experience
            print(f"Новый опыт: {new_year_experience} (в годах)")
        elif new_year_experience == self._experience:
            print(f"Опыт не изменился")
        elif new_year_experience > (int(self._experience) + 2):
            print("Нельзя делать опыт больше чем на 2 года")
        elif new_year_experience < int(self._experience):
            print("Нельзя делать опыт меньше")

    @property
    def salary(self):
        return f"Текущая зарплата подопытного - {self._salary}$"

    @salary.setter
    def salary(self, new_value_salary):
        """ При изменение запрплаты выводить радостное сообщение, если больше чем было и грустное, если меньше. """
        if new_value_salary > int(self._salary):
            print(f" :) Хорошие новости! Новая зарплата: {new_value_salary}$!")
        elif new_value_salary < (int(self._salary)):
            print(f" :( Плохие новости! Новая зарплата: {new_value_salary}$! ")
        elif new_value_salary == self._salary:
            print(f"Нипанятнаааа! Зарплата не изменилась -  {new_value_salary}$! Хорошо, что не меньше! ")
        self._salary = new_value_salary

    def do_testing(self):
        """ Класс содежит метод do_testing, котрый выводит "Начинаю тестирование...". """
        print("Начинаю тестирование...")


class ManualQualityAssuranceEngineer(QualityAssuranceEngineer):
    """
    От QualityAssuranceEngineer отнаследовать класс ManualQualityAssuranceEngineer.
    В нем переопределить метод do_testing, он должен выводить "Эх, погнал тестировать руками...".
    """

    def __init__(self, experience, salary):
        super().__init__(experience, salary)

    def do_testing(self):
        """ Должен выводить "Эх, погнал тестировать руками...". """
        print("Эх, погнал тестировать руками...")


class AutomationQualityAssuranceEngineer(QualityAssuranceEngineer):
    """
    Также от основного класса отнаследовать класс AutomationQualityAssuranceEngineer.
    Его конструктор должен принимать дополнительный параметр code_lang.
    Метод do_testing переопределить что бы он выводил "Ха, щя поставлю раниться тесты на /code_lang/ и го пить кофе )"
    """

    def __init__(self, experience, salary, code_lang):
        """
        ...от основного класса отнаследовать класс AutomationQualityAssuranceEngineer.Конструктор должен
        принимать дополнительный параметр code_lang.
        """
        super().__init__(experience, salary)
        self.code_lang = code_lang

    def do_testing(self):
        """
        Метод do_testing переопределить что бы он выводил "Ха, щa поставлю раниться тесты на /code_lang/ и го
        пить кофе )"
        """
        print(f"Ха, ща поставлю раниться тесты на {self.code_lang} и го пить кофе )")


qa = QualityAssuranceEngineer(3, 1000)
print(qa.experience)
qa.experience = 7
qa.experience = 1
qa.experience = 3
qa.experience = 4
print(qa.experience)
qa.salary = 1000
qa.salary = 2000
qa.salary = 100
print("=" * 30)
print(qa.salary)
print("=" * 30)
qa.do_testing()
print("=" * 30)
print("=" * 30)
print("=" * 30)

manual_qa = ManualQualityAssuranceEngineer(1, 800)
print(manual_qa.experience)
manual_qa.experience = 7
manual_qa.experience = 1
manual_qa.experience = 4
manual_qa.experience = 3
print(manual_qa.experience)
manual_qa.salary = 800
manual_qa.salary = 2000
manual_qa.salary = 100
print("=" * 30)
print(manual_qa.salary)
print("=" * 30)
manual_qa.do_testing()
print("=" * 30)
print("=" * 30)
print("=" * 30)

aqa = AutomationQualityAssuranceEngineer(0, 2200, 'Python')
print(aqa.experience)
aqa.experience = 7
aqa.experience = 1
aqa.experience = 4
aqa.experience = 2
print(aqa.experience)
aqa.salary = 800
aqa.salary = 2000
aqa.salary = 100
print("=" * 30)
print(aqa.salary)
print("=" * 30)
aqa.do_testing()
print("=" * 30)
print("=" * 30)
print("=" * 30)
print("=" * 30)
