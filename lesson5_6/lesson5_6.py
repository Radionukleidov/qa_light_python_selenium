"""Lesson 5-6 16-03-2021 вторник"""

# 1
print('# 1')
for x in range(0, 10):
    print(x)

print("=" * 30)

# 2
print('# 2')
for x in range(0, 10):
    if x == 2:
        continue
    if x == 7:
        break
    print(x)

print("=" * 30)

# 3
print('# 3')
for x in range(0, 10):
    if x == 2:
        continue
    if x == 13:
        break
    print(x)
else:
    print('We are here - не дошли до break - выводим else')  # используется если мы не дошли до break

print("=" * 30)

# 4
print('# 4')


def full_name(name, surname):
    print(name + surname)


full_name(surname='2', name='1')  # можно менять местами именованные
full_name('1', '2')  # нельзя менять местами именованные

print("=" * 30)

# 5
print('# 5')


class ClassName:
    '''Documentation'''
    pass


obj = ClassName()

print("=" * 30)

# 6
print('# 6')


class Car:
    def __init__(self, color,
                 weight):  # перезаписываем создание класса. self - это сам класс, остальное - то что мы хотим добавить цвет и вес
        self.color = color
        self.weight = weight
        # класс при создании принимает цвет и вес и принимает их  в себя

    def func(cls):
        pass

    @staticmethod  # ничего связанного с классом не делает, просто лежит здесь
    def func():
        pass


volvo = Car(color='black', weight='1500')
honda = Car(color='black', weight='1500')
print(volvo)
print('color - ', volvo.color)
print('color - ', honda.color)

volvo.color = 'red'
print('color - ', volvo.color)
print('color - ', honda.color)

print("=" * 30)

# 7
print('# 7')


def name_of_func(param_1='1', param_2='2'):  # может быть с дефолтными, может без
    """
    Same description
    params: param_1 (str) Some reason
    params: param_2 (str) Some reason
    :return:
    """

    print('param_1 - ', param_1)
    print('param_2 - ', param_2)
    return param_1 + param_2


name_of_func()

name_of_func('17', '13')


def sum(number1, number2):
    summ = number1 + number2
    return summ


summ = sum(1, 2)
print(summ)

print("=" * 30)

# 8
print('# 8')
print('Самостоятельная')
'''Создать класс, который при инициализации принимает число, где один метод выводит половину числа, 
второй метод возвращает число в 2 раза больше'''


class MathActions:
    """Do some math actions"""

    def __init__(self, number):
        self.number_1 = number

    def half(self):
        """выводит половину числа"""
        print(self.number_1 / 2)

    def x2(self):
        """возвращает число в 2 раза больше"""
        return self.number_1 * 2


math_acts = MathActions(number=60)
math_acts.half()
x2 = math_acts.x2()
print(x2)

print("=" * 30)

# 9
print('# 9')
