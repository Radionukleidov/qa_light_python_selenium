import pytest
import random
import string
import logging
from lesson21_22.homework_lesson21_22.conftest import BaseTest


# 6. Создать класс RegisterPage со следующими свойствами:
# - Логин, значние по умолчанию - пустая строка
# - Пароль, значние по умолчанию - пустая строка
# - Повторите пароль, значение по умолчанию - пустая строка
# - Почта, значение по умолчанию - пустая строка
# - sign_up_enabled - значение по умолчанию False
# Значение sign_up_enabled меняется только когда все 4 поля заполненны.
# Логин должен быть длиннее чем 5 символов, иначе ошибка
# Пароль должен содержать не только буквы, иначе ошибка
# Повторите пароль должен быть равен паролю, иначе ошибка.
# Почта должна содержать собачку, иначе ошибка.
# Написать тесты на это класс.

'''
Домашка с занятия 13-04-2021

На основании прошлого домашнего задания добавить:
++ Пару тестов (если используете мой вариант)
++ Логирование - https://webdevblog.ru/logging-v-python/
++ Фикстуру уровня класса, которая будет возвращать объект нашей “страницы”
++ Фикстуру уровня функции, которая будет заполнять одно из “полей” перед тестом и чистить его после.
++ Обновить тесты с использованием этих фикстур.
++ *Проверить что тесты проходят в любом порядке.
Домашка небольшая, основу в виде прошлой домашки я вам  скинул. Так что просьба всем хотя бы попробовать и прийти с
вопросами, если они будут )

'''


class RandomData:
    """This class uses methods to generate random values"""

    @staticmethod
    def random_valid_login():
        """Generator random login"""
        login_rand_value = "".join(random.choice(string.ascii_letters) for _ in range(random.randint(6, 15)))
        return login_rand_value

    @staticmethod
    def random_valid_password():
        """Generator random password"""
        password_rand_value = "".join(random.choice(string.ascii_letters) + random.choice(string.digits)
                                      for _ in range(random.randint(7, 15)))
        return password_rand_value

    @staticmethod
    def random_valid_email():
        """Generator random email"""
        email = "autotestqaboss+" + str(random.randint(100, 9000000)) + "@gmail.com"
        return email

    @staticmethod
    def random_valid_age():
        """Generator random age (from 18 to 99)"""
        age = random.randint(18, 99)
        return age

    @staticmethod
    def random_valid_gender():
        """Generator random gender ("Male" or "Female")"""
        gender = ["Male", "Female"]
        random_gender = random.choice(gender)
        return random_gender


class RegisterPage:

    def __init__(self):
        self._login = ""
        self._password = ""
        self._confirm_password = ""
        self._email = ""
        self._sign_up_enabled = False
        self._age = ""
        self._gender = ""

    @property
    def login(self):
        return self._login

    @login.setter
    def login(self, new_login):
        if len(new_login) < 5:
            raise ValueError("Логин должен быть длиннее чем 5 символов")
        else:
            self._login = new_login

    @login.deleter
    def login(self):
        self._login = ""
        self._sign_up_enabled = False

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, new_password):
        if new_password.isalpha():
            raise ValueError("Пароль должен содержать не только буквы")
        else:
            self._password = new_password

    @password.deleter
    def password(self):
        self._password = ""
        self._sign_up_enabled = False

    @property
    def confirm_password(self):
        return self._confirm_password

    @confirm_password.setter
    def confirm_password(self, confirm_password):
        if confirm_password != self._password:
            raise ValueError("Подтверждение пароля должно быть равно паролю")
        else:
            self._confirm_password = confirm_password

    @confirm_password.deleter
    def confirm_password(self):
        self._confirm_password = ""
        self._sign_up_enabled = False

    @property
    def email(self):
        return self._email

    @email.setter
    def email(self, new_email):
        if "@" not in new_email:
            raise ValueError("Почта должна содержать собачку")
        else:
            self._email = new_email

    @email.deleter
    def email(self):
        self._email = ""
        self._sign_up_enabled = False

    @property
    def sign_up_enabled(self):
        if self._email \
                and self._password \
                and self._confirm_password \
                and self._login \
                and self._age \
                and self._gender:
            self._sign_up_enabled = True
        else:
            self._sign_up_enabled = False
        return self._sign_up_enabled

    @property
    def age(self):
        return self._age

    @age.setter
    def age(self, new_age):
        if new_age < 18:
            raise ValueError("Возраст должен быть равен или больше 18")
        else:
            self._age = new_age

    @age.deleter
    def age(self):
        self._age = ""
        self._sign_up_enabled = False

    @property
    def gender(self):
        return self._gender

    @gender.setter
    def gender(self, new_gender):
        if "Female" not in new_gender and "Male" not in new_gender:
            raise ValueError('Пол должен быть "Male" или "Female"')
        else:
            self._gender = new_gender

    @gender.deleter
    def gender(self):
        self._gender = ""
        self._sign_up_enabled = False


class TestRegisterPage(BaseTest):
    # logger = logging.getLogger(__name__)

    @pytest.fixture(scope='class')
    def register_page(self):
        self.logger.debug("Register page was modified")
        register_page = RegisterPage()
        return register_page

    @pytest.fixture(scope='function')
    def set_login(self, register_page):
        self.logger.debug("Preparing login...")
        self.test_page = register_page
        self.test_page.login = RandomData.random_valid_login()
        yield
        del self.test_page.login
        self.logger.debug("Cleanup login...")

    @pytest.fixture(scope='function')
    def set_password(self, register_page):
        self.logger.debug("Preparing password...")
        self.test_page = register_page
        self.test_page.password = "weqyu342142yewqwoo"
        yield
        del self.test_page.password
        self.logger.debug("Cleanup password...")

    @pytest.fixture(scope='function')
    def set_confirm_password(self, register_page):
        self.logger.debug("Preparing confirm_password...")
        self.test_page = register_page
        self.test_page.confirm_password = "weqyu342142yewqwoo"
        yield
        del self.test_page.confirm_password
        self.logger.debug("Cleanup confirm_password...")

    @pytest.fixture(scope='function')
    def set_email(self, register_page):
        self.logger.debug("Preparing email...")
        self.test_page = register_page
        self.test_page.email = RandomData.random_valid_email()
        yield
        del self.test_page.email
        self.logger.debug("Cleanup email...")

    @pytest.fixture(scope='function')
    def set_age(self, register_page):
        self.logger.debug("Preparing age...")
        self.test_page = register_page
        self.test_page.age = RandomData.random_valid_age()
        yield
        del self.test_page.age
        self.logger.debug("Cleanup age...")

    @pytest.fixture(scope='function')
    def set_gender(self, register_page):
        self.logger.debug("Preparing gender...")
        self.test_page = register_page
        self.test_page.gender = RandomData.random_valid_gender()
        yield
        del self.test_page.gender
        self.logger.debug("Cleanup gender...")

    def test_defaults(self, register_page):
        """
        - Create register page
        - Verify all default values
        """
        assert register_page.login == ""
        assert register_page.password == ""
        assert register_page.confirm_password == ""
        assert register_page.email == ""
        assert register_page.age == ""
        assert register_page.gender == ""
        assert not register_page.sign_up_enabled
        self.logger.info("All default values was verified - [Sign Up] is disabled")

    def test_fill_all_fields(self, register_page, set_login, set_password, set_confirm_password, set_email, set_age,
                             set_gender):
        """
        - Create register page
        - Fill all fields
        - Verify that sign_up_enabled became True
        """

        # Fill fields
        assert self.test_page.login
        assert self.test_page.password
        assert self.test_page.confirm_password
        assert self.test_page.email
        assert self.test_page.age
        assert self.test_page.gender

        # Verify that button enabled
        assert register_page.sign_up_enabled
        self.logger.info("All fields filled - [Sign Up] is enabled")

    def test_invalid_login(self, register_page):
        """
        - Negative test
        - Create register page
        - Try to set invalid login
        - Verify that exception exists
        - Verify that login doesn't set
        """

        # Fill fields
        incorrect_login = "user"
        try:
            register_page.login = incorrect_login
        except ValueError:
            pass
        else:
            raise AssertionError("Логин должен быть длиннее чем 5 символов")

        # Verify that login doesn't set
        assert not register_page.login
        self.logger.info(f"Login negative value - all is ok, '{incorrect_login}' - not allowed.")

    @pytest.mark.order(2)
    def test_invalid_age(self, register_page):
        """
        - Negative test
        - Create register page
        - Try to set invalid age (< 18)
        - Verify that exception exists
        - Verify that age doesn't set
        """
        incorrect_age = 11
        try:
            register_page.age = incorrect_age
        except ValueError:
            pass
        else:
            raise AssertionError("Возраст должен быть равен или больше 18")

        # Verify that age doesn't set
        assert not register_page.age, f"Oops, something went wrong, age is set"
        self.logger.info(f"Age negative value - all is ok, '{incorrect_age}' - not allowed.")

    def test_fill_not_all_fields(self, set_password, set_confirm_password, set_email, set_age, set_gender):
        """
        - Negative test
        - Create register page
        - Fill all fields without 'login'
        - Verify that sign_up_enabled became False
        """

        # Fill fields without 'login'
        assert not self.test_page.login
        self.logger.info("An empty login field - verification completed")
        assert self.test_page.password
        assert self.test_page.confirm_password
        assert self.test_page.email
        assert self.test_page.age
        assert self.test_page.gender

        # Verify that button disabled
        assert not self.test_page.sign_up_enabled, f"Oops, something went wrong - [Sign up] is enabled"
        self.logger.info("An empty login field - [Sign Up] is disabled")

    @pytest.mark.order(1)
    def test_invalid_gender(self, register_page):
        """
        - Negative test
        - Create register page
        - Try to set invalid gender (not in "Male" or "Female")
        - Verify that exception exists
        - Verify that gender doesn't set
        """
        incorrect_gender = "Мужчина"
        try:
            register_page.gender = incorrect_gender
        except ValueError:
            pass
        else:
            raise AssertionError('Пол должен быть "Male" или "Female"')

        # Verify that gender doesn't set
        assert not register_page.gender, f"Oops, something went wrong, gender is set"
        self.logger.info(f"Gender negative value - all is ok, '{incorrect_gender}' - not allowed.")
