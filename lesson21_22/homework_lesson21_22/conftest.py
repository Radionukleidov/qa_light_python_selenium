import logging


def pytest_runtest_setup(item):
    """Prepare test"""
    log = logging.getLogger(item.name)
    item.cls.logger = log


class BaseTest:
    """BaseTest class for inheritance. Implements test class default variables."""
    # Defined to fix `unresolved attribute` warning
    # Default values to provide autocomplete
    logger = logging.getLogger(__name__)
