import pytest
import logging

from lesson21_22.conftest import BaseTest


class LoginPage:

    def __init__(self):
        self._sing_in_enabled = False
        self._login = ''
        self._password = ''

    @property
    def sign_in_enabled(self):
        return self._sing_in_enabled

    @sign_in_enabled.setter
    def sign_in_enabled(self, sign_in_enabled):
        self._sing_in_enabled = sign_in_enabled

    @property
    def login(self):
        return self._login

    @login.setter
    def login(self, login):
        self._login = login
        if self._login and self._password:
            self.sign_in_enabled = True
        else:
            self.sign_in_enabled = False

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        self._password = password
        if self._login and self._password:
            self.sign_in_enabled = True
        else:
            self.sign_in_enabled = False


class TestLoginPage(BaseTest):

    @pytest.fixture(scope='class')
    def open_page(self):
        self.logger.debug('Class level')
        return LoginPage()

    @pytest.fixture(scope='function')
    def set_login(self, open_page):
        self.logger.debug("Preparing login...")
        self.page = open_page
        self.page.login = 'user@mail.com'
        yield
        self.logger.debug("Cleanup login...")
        self.page.login = ''

    @pytest.fixture(scope='function')
    def set_password(self, open_page):
        self.logger.debug("Preparing password..")
        self.page = open_page
        self.page.password = '1234'
        yield
        self.logger.debug("Cleanup password...")
        self.page.password = ''

    def test_default(self, open_page):
        assert not open_page.login
        assert not open_page.password
        assert not open_page.sign_in_enabled
        self.logger.info("Default state was verified")

    def test_one_field(self, set_login):
        assert self.page.login
        assert not self.page.password
        assert not self.page.sign_in_enabled
        self.logger.info("One field state was verified")

    @pytest.mark.enabled
    def test_enabled(self, set_login, set_password):
        assert self.page.login
        assert self.page.password
        assert self.page.sign_in_enabled
        self.logger.info("All fields state was verified")
