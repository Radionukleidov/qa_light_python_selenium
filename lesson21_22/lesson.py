"""Lesson"""

from datetime import datetime
from time import sleep

import pytest


@pytest.fixture()
def current_date():
    """Return actual date"""
    return datetime.today().weekday()


@pytest.fixture(autouse=True, scope="class")
def configure():
    print("Hi, we're starting now...")
    yield
    print("Finishing...")


class TestDay:
    def test_lesson_day(self, current_date):
        """Verify lesson day"""
        sleep(3)
        assert current_date in [1, 3]

    def test_lesson_day_2(self, current_date):
        """Verify lesson day"""
        sleep(3)
        assert current_date not in [0, 2, 4, 5, 6]


def test_lesson_day_func(current_date):
    """Verify lesson day"""
    sleep(3)
    assert current_date in [1, 3]


assert all([i % 2 == 0 for i in [2, 4, 6, 8]])
print(all([0, 2, 4, 6, 8, 10, 12]))
print(False % 2)
assert all([0, 2, 4, 6, 8, 10, 12]) % 2 == 0
